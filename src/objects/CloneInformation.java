package objects;

import dto.ProxyDTO;

import java.util.Date;

public class CloneInformation {
    long accountId;
    String uid;
    String password;
    String realName;
    String email;
    String phone;
    String birthday;
    String address;
    String cookies;
    ProxyDTO proxyDTO;

    public ProxyDTO getProxyDTO() {
        return proxyDTO;
    }

    public void setProxyDTO(ProxyDTO proxyDTO) {
        this.proxyDTO = proxyDTO;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }
}
