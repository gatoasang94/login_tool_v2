package objects;

public class Accounts {
    String workingUID;
    String trust1;
    String trust2;
    String trust3;
    String trust4;
    String trust5;

    public Accounts(){

    }

    public Accounts(String workingUID, String trust1, String trust2, String trust3, String trust4, String trust5) {
        this.workingUID = workingUID;
        this.trust1 = trust1;
        this.trust2 = trust2;
        this.trust3 = trust3;
        this.trust4 = trust4;
        this.trust5 = trust5;
    }

    public String getWorkingUID() {
        return workingUID;
    }

    public void setWorkingUID(String workingUID) {
        this.workingUID = workingUID;
    }

    public String getTrust1() {
        return trust1;
    }

    public void setTrust1(String trust1) {
        this.trust1 = trust1;
    }

    public String getTrust2() {
        return trust2;
    }

    public void setTrust2(String trust2) {
        this.trust2 = trust2;
    }

    public String getTrust3() {
        return trust3;
    }

    public void setTrust3(String trust3) {
        this.trust3 = trust3;
    }

    public String getTrust4() {
        return trust4;
    }

    public void setTrust4(String trust4) {
        this.trust4 = trust4;
    }

    public String getTrust5() {
        return trust5;
    }

    public void setTrust5(String trust5) {
        this.trust5 = trust5;
    }
}
