/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.google.gson.Gson;
import dto.*;
import objects.CloneInformation;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import util.common.Constant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static util.common.Constant.HTTP.*;

/**
 * @author Thai Tuan Anh
 */
public class BaseService {


    public ConnectionLog sendGetRequest(String url) throws MalformedURLException, IOException {
        ConnectionLog result = new ConnectionLog();
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        result.setMessage(response.toString());
        result.setErrorCode(ERROR_CODE_SUCCESS);

        return result;
    }

    public ConnectionLog sendPostRequest(String url, String msgBody) throws Exception {
        ConnectionLog result = new ConnectionLog();
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        post.setHeader("User-Agent", USER_AGENT);
        post.setHeader("Accept-Language", ACCEPTED_LANGUAGE);
        post.setHeader("Content-Type", CONTENT_TYPE);

        StringEntity entity = new StringEntity(msgBody, "UTF-8");
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        result.setMessage(res.toString());
        result.setErrorCode(ERROR_CODE_SUCCESS);

        return result;
    }

    public WebConnectionLog sendPostRequestToWebServer(String jwt, String url, Object object) throws Exception {
        WebConnectionLog result = new WebConnectionLog();
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        Gson gson = new Gson();

        if (jwt != null) {
            post.setHeader("Authorization", jwt);
        }
        post.setHeader("Content-Type", "application/json");

        StringEntity entity = new StringEntity(gson.toJson(object));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        result = gson.fromJson(response.toString(), WebConnectionLog.class);

        return result;
    }

    public WebLoginDTO logInToWebServer(Object object) throws Exception {
        WebLoginDTO result = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(Constant.WEB.WEB_ADDRESS + "/users/authen");
        Gson gson = new Gson();

        post.setHeader("Content-Type", "application/json");

        StringEntity entity = new StringEntity(gson.toJson(object));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));


        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        result = gson.fromJson(res.toString(), WebLoginDTO.class);

        return result;
    }

    public WebConnectionLog sendObjectToWebServer(Object object, String url, String token) throws Exception {
        WebConnectionLog result = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        Gson gson = new Gson();

        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", token);

        StringEntity entity = new StringEntity(gson.toJson(object));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));


        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        result = gson.fromJson(res.toString(), WebConnectionLog.class);

        return result;
    }

    public CloneInformation getCloneInformation(String uid, String token) throws Exception {
        CloneInformation result = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(Constant.WEB.WEB_ADDRESS + "/logintool/getCloneInfo");
        Gson gson = new Gson();

        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", token);

        CloneUidToSvDTO cloneUidToSvDTO = new CloneUidToSvDTO();
        cloneUidToSvDTO.setUid(uid);

        StringEntity entity = new StringEntity(gson.toJson(cloneUidToSvDTO));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));


        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        result = gson.fromJson(res.toString(), CloneInformation.class);

        return result;
    }

    public ProxyDTO changeCloneProxy(String uid, String token) throws Exception {
        ProxyDTO result = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(Constant.WEB.WEB_ADDRESS + "/logintool/getnewproxyforclone");
        Gson gson = new Gson();

        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", token);

        CloneUidToSvDTO cloneUidToSvDTO = new CloneUidToSvDTO();
        cloneUidToSvDTO.setUid(uid);

        StringEntity entity = new StringEntity(gson.toJson(cloneUidToSvDTO));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));


        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }

        result = gson.fromJson(res.toString(), ProxyDTO.class);

        return result;
    }

    public LoginAccountBundleStcDTO checkCloneUidAndGetProxy(String uid, String token) throws Exception {
        LoginAccountBundleStcDTO result = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(Constant.WEB.WEB_ADDRESS + "/logintool/loginnewclone");
        Gson gson = new Gson();

        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", token);

        CloneUidToSvDTO cloneUidToSvDTO = new CloneUidToSvDTO();
        cloneUidToSvDTO.setUid(uid);

        StringEntity entity = new StringEntity(gson.toJson(cloneUidToSvDTO));
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));


        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            res.append(line);
        }
        if (res.toString().contains("Unauthorized")) {

        } else {
            result = gson.fromJson(res.toString(), LoginAccountBundleStcDTO.class);
        }

        return result;
    }
}
