/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dto.BaseDTO;
import dto.BaseLog;
import dto.ConnectionLog;
import util.common.Constant;
import util.common.DataUtil;

/**
 * @author Thai Tuan Anh
 */
public class ServerService extends BaseService {

    public static String sendDTOToServer(String serverUrl, BaseDTO dto) {
        try {
            BaseService ws = new BaseService();
            ConnectionLog connectionLog = (ConnectionLog) DataUtil.jsonStringToObject(ws.sendPostRequest(serverUrl, DataUtil.objectToJsonString(dto)).getMessage(), ConnectionLog.class);

            return connectionLog.getErrorCode();
        } catch (Exception ex) {
            System.out.println("GET DTO FROM SERVER ERROR!");
            return Constant.HTTP.ERROR_CODE_FAIL;
        }
    }

    public static String sendToServer(String serverUrl) {
        try {
            BaseService ws = new BaseService();
            String actionResult = ws.sendGetRequest(serverUrl).getMessage();

            return actionResult;
        } catch (Exception ex) {
            System.out.println("GET INFO FROM SERVER ERROR!");
            return Constant.HTTP.ERROR_CODE_FAIL;
        }
    }

    public static String sendLogToServer(String serverUrl, BaseLog log) {
        try {
            BaseService ws = new BaseService();
            ConnectionLog connectionLog = (ConnectionLog) DataUtil.jsonStringToObject(ws.sendPostRequest(serverUrl, DataUtil.objectToJsonString(log)).getMessage(), ConnectionLog.class);

            return connectionLog.getErrorCode();
        } catch (Exception ex) {
            return Constant.HTTP.ERROR_CODE_FAIL;
        }
    }





    public static String sendObjectToServer(String serverUrl, Object obj) {
        try {
            BaseService ws = new BaseService();
            ConnectionLog connectionLog = (ConnectionLog) DataUtil.jsonStringToObject(ws.sendPostRequest(serverUrl, obj.toString()).getMessage(), ConnectionLog.class);

            return connectionLog.getErrorCode();
        } catch (Exception ex) {
            return Constant.HTTP.ERROR_CODE_FAIL;
        }
    }
}
