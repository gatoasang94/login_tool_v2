package main;

import base.Driver;
import dto.LoggedAccountDTO;
import dto.ProxyDTO;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import objects.Accounts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import service.BaseService;
import util.common.DataUtil;

import java.util.List;

import static util.common.Constant.SERVER.SERVER_ACCOUNT_REPORT_V2;

public class CloneOpenerWithCookie extends Thread implements Runnable {
    ObservableList<Accounts> baseAccountList;
    int baseAccountListIndex;
    ProxyDTO proxyDTO;
    String workingUID;
    String workingCookie;
    TableView<Accounts> table;
    long workingProxyId;
    Driver driver;

    public CloneOpenerWithCookie(ProxyDTO proxyDTO, String workingUID, long workingProxyId, String workingCookie) {
        this.proxyDTO = proxyDTO;
        this.workingUID = workingUID;
        this.workingProxyId = workingProxyId;
        this.workingCookie = workingCookie;
    }

    @Override
    public void run() {
        try {
            driver = new Driver();
            driver.driverWithProxyAndCookie(proxyDTO, workingCookie);
        } catch (Exception ignored) {
        }
    }

    private void cloneCheckPointDetector(String trustUID) {
        try {
            for (int i=0;i<1000000;i++) {
                String currentURL = driver.getDriver().getCurrentUrl();
                List<WebElement> fbNotificationsJewelList = driver.getDriver().findElements(By.id("fbNotificationsJewel"));
                if (currentURL.contains("checkpoint")) {

                } else if (!fbNotificationsJewelList.isEmpty()) {
                    workingCookie = driver.getCookies();
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {
        } finally {
        }
    }
}