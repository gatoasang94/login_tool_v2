package main;

import base.Driver;
import dto.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import service.BaseService;
import util.common.Constant;
import util.common.DataUtil;

import java.io.IOException;


public class AddCloneNormalScene extends Main{
    public AddCloneNormalScene(String token){
        super.token = token;
    }

    private Driver driver;
    private Button addNewCookieButton, doneButton, imgCheckPointSupportButton, cmtCheckPointSupportButton, ibCheckPointSupportButton, returnHomeButton, cancelButton;
    private TextField cookieField, passwordField, backUpFileDir;
    private boolean isGoodCookie = false;
    private HBox cookieHbox, pwEmailHbox, cmtIbCpHbox, imgHbox, returnHomeHbox;
    private VBox mainVbox;
    String workingCookie;
    Long workingProxyId;

    public Scene display(Stage window, Scene homeScene) throws IOException {

        driver = new Driver();


        addNewCookieButton = new Button("Thêm Mới");
        addNewCookieButton.setMinHeight(30);
        addNewCookieButton.setMinWidth(100);
        addNewCookieButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            addNewCookieButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        doneButton = new Button("Xong");
        doneButton.setMinHeight(30);
        doneButton.setMinWidth(100);
        doneButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            doneButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        imgCheckPointSupportButton = new Button("Mở CheckPoint Bằng Ảnh");
        imgCheckPointSupportButton.setMinHeight(30);
        imgCheckPointSupportButton.setMinWidth(100);
        imgCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            imgCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        cmtCheckPointSupportButton = new Button("Mở CheckPoint Bằng Bình Luận");
        cmtCheckPointSupportButton.setMinHeight(30);
        cmtCheckPointSupportButton.setMinWidth(100);
        cmtCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cmtCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        ibCheckPointSupportButton = new Button("Mở CheckPoint Bằng Tin Nhắn");
        ibCheckPointSupportButton.setMinHeight(30);
        ibCheckPointSupportButton.setMinWidth(100);
        ibCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ibCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        returnHomeButton = new Button("Home");
        returnHomeButton.setMinHeight(30);
        returnHomeButton.setMinWidth(100);
        returnHomeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            returnHomeButtonClicked(window, homeScene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        cancelButton = new Button("Hủy");
        cancelButton.setMinHeight(30);
        cancelButton.setMinWidth(100);
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cancelButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        addNewCookieButton.disableProperty().setValue(false);
        doneButton.disableProperty().setValue(true);
        imgCheckPointSupportButton.disableProperty().setValue(true);
        cmtCheckPointSupportButton.disableProperty().setValue(true);
        ibCheckPointSupportButton.disableProperty().setValue(true);
        cancelButton.disableProperty().setValue(true);

        cookieField = new TextField();
        cookieField.setPromptText("Cookie");
        cookieField.setMinWidth(150);
        cookieField.setMinHeight(30);

        passwordField = new TextField();
        passwordField.setPromptText("Password");
        passwordField.setMinWidth(150);
        passwordField.setMinHeight(30);


        backUpFileDir = new TextField();
        backUpFileDir.setPromptText("Đường dẫn file backup *.html");
        backUpFileDir.setMinWidth(250);
        backUpFileDir.setMinHeight(30);


        cookieHbox = new HBox();
        cookieHbox.setPadding(new Insets(10, 20, 10, 20));
        cookieHbox.setSpacing(20);
        cookieHbox.getChildren().addAll(cookieField, addNewCookieButton, cancelButton);

        pwEmailHbox = new HBox();
        pwEmailHbox.setPadding(new Insets(10, 20, 10, 20));
        pwEmailHbox.setSpacing(20);
        pwEmailHbox.getChildren().addAll(passwordField, doneButton);

        cmtIbCpHbox = new HBox();
        cmtIbCpHbox.setPadding(new Insets(10, 20, 10, 20));
        cmtIbCpHbox.setSpacing(20);
        cmtIbCpHbox.getChildren().addAll(cmtCheckPointSupportButton, ibCheckPointSupportButton);

        imgHbox = new HBox();
        imgHbox.setPadding(new Insets(10, 20, 10, 20));
        imgHbox.setSpacing(20);
        imgHbox.getChildren().addAll(backUpFileDir, imgCheckPointSupportButton);

        returnHomeHbox = new HBox();
        returnHomeHbox.setPadding(new Insets(10, 20, 10, 20));
        returnHomeHbox.setSpacing(20);
        returnHomeHbox.getChildren().addAll(returnHomeButton);

        resetStage();

        mainVbox = new VBox();
        mainVbox.getChildren().addAll(returnHomeHbox, cookieHbox, pwEmailHbox, cmtIbCpHbox, imgHbox);
        Scene addCloneScene = new Scene(mainVbox);
        return addCloneScene;
    }

    private void addNewCookieButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();

        workingCookie = cookieField.getText();
//        passwordField.setText();
//        emailField.setText();
        LoginAccountBundleStcDTO loginAccountBundleStcDTO = baseService.checkCloneUidAndGetProxy(getUidFromCookie(workingCookie), token);

        if (loginAccountBundleStcDTO.getWorkingUID() != null && loginAccountBundleStcDTO.getWorkingUID().equals("na")) {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window,"Cookie Problem", "Acc Đã Có Trên Hệ Thống");
            cookieField.clear();
        } else {
            if (loginAccountBundleStcDTO.getProxyDTO().getProxyId() == -1) {
                AlertBox alertBox = new AlertBox();
                alertBox.display(window,"PROXY Problem", "HẾT PROXY - GỌI 01644517304");
            } else {
                workingStage();
                passwordField.setText("122331");
                workingProxyId = loginAccountBundleStcDTO.getProxyDTO().getProxyId();
                driver.driverWithProxyAndCookie(loginAccountBundleStcDTO.getProxyDTO(), workingCookie);
            }
        }
    }


    public static String getUidFromCookie(String cookie) {
        if (!DataUtil.isNullOrEmptyStr(cookie)) {
            if (cookie.contains("\n")) {
                cookie = cookie.replaceAll("\n", ";");
            }

            if (cookie.contains(":")) {
                cookie = cookie.replaceAll(":", "=");
            }

            String[] keyValues = cookie.split(";");

            if (keyValues != null && keyValues.length > 0) {
                for (String keyValue : keyValues) {
                    if (keyValue.contains("c_user:")) {
                        return keyValue.split(":")[1];
                    } else if (keyValue.contains("c_user=")) {
                        return keyValue.split("=")[1];
                    }
                }
            }
        }

        return null;
    }

    private void doneButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();
        LoggedCloneToSvDTO loggedCloneToSvDTO = new LoggedCloneToSvDTO();

        workingCookie = driver.getCookies();
        loggedCloneToSvDTO.setUid(getUidFromCookie(workingCookie));
        loggedCloneToSvDTO.setCookies(workingCookie);
        loggedCloneToSvDTO.setPassword(passwordField.getText());
        loggedCloneToSvDTO.setProxyId(workingProxyId);

        WebConnectionLog webConnectionLog = baseService.sendObjectToWebServer(loggedCloneToSvDTO, Constant.WEB.WEB_ADDRESS + "/logintool/loggednewclone", token);

        if (webConnectionLog.isSuccess()) {
            resetStage();
            driver.getDriver().quit();
        } else {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window,"Sending Problem", webConnectionLog.getMsg());
        }
    }

    private void imgCheckPointSupportButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window,"Thông Báo", "Chưa Làm Đâu :v");
    }

    private void cmtCheckPointSupportButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window,"Thông Báo", "Chưa Làm Đâu :v");
    }

    private void ibCheckPointSupportButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window,"Thông Báo", "Chưa Làm Đâu :v");
    }

    private void returnHomeButtonClicked(Stage window, Scene homeScene) {
        window.setAlwaysOnTop(false);
        window.setScene(homeScene);
    }

    private void cancelButtonClicked() {
        resetStage();
        try{
            driver.getDriver().quit();
        } catch (Exception ignored){

        }
    }

    private void resetStage() {
        isGoodCookie = false;
        cookieField.clear();
        passwordField.clear();
        workingCookie = null;
        workingProxyId = null;

        passwordField.setEditable(false);
        backUpFileDir.setEditable(false);
        cookieField.setEditable(true);

        addNewCookieButton.disableProperty().setValue(false);
        doneButton.disableProperty().setValue(true);
        imgCheckPointSupportButton.disableProperty().setValue(true);
        cmtCheckPointSupportButton.disableProperty().setValue(true);
        ibCheckPointSupportButton.disableProperty().setValue(true);
        cancelButton.disableProperty().setValue(true);
    }

    private void workingStage() {
        isGoodCookie = true;

        passwordField.setEditable(true);
        backUpFileDir.setEditable(true);
        cookieField.setEditable(false);

        addNewCookieButton.disableProperty().setValue(true);
        doneButton.disableProperty().setValue(false);
        imgCheckPointSupportButton.disableProperty().setValue(false);
        cmtCheckPointSupportButton.disableProperty().setValue(false);
        ibCheckPointSupportButton.disableProperty().setValue(false);
        cancelButton.disableProperty().setValue(false);
    }
}