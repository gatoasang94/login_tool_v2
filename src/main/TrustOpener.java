package main;

import base.Driver;
import dto.LoginAccountBundleStcDTO;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import objects.Accounts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static util.common.Constant.ACTION.LOAD_TIME;
import static util.common.DataUtil.random;

public class TrustOpener extends Thread implements Runnable {
    ObservableList<Accounts> baseAccountList;
    int baseAccountListIndex;
    LoginAccountBundleStcDTO.TrustObject trustObject;
    TableView<Accounts> table;
    Driver driver;

    public TrustOpener(ObservableList<Accounts> baseAccountList, int baseAccountListIndex, LoginAccountBundleStcDTO.TrustObject trustObject, TableView<Accounts> table) {
        this.baseAccountList = baseAccountList;
        this.baseAccountListIndex = baseAccountListIndex;
        this.trustObject = trustObject;
        this.table = table;
    }

    @Override
    public void run() {
        try {
            driver = new Driver();
            if (trustObject.getCookies().equals("na")) {
                driver.driverFor3G();
                WebElement uidInputElements = driver.getDriver().findElement(By.xpath(".//input[@data-testid='royal_email']"));
                uidInputElements.sendKeys(trustObject.getTrust());
                trustCheckPointDetector(trustObject.getTrust());
            } else {
                driver.driverWithProxyAndCookie(trustObject.getProxyDTO(), trustObject.getCookies());
                trustCheckPointDetector(trustObject.getTrust());
            }
        } catch (Exception ignored) {
        }
    }

    private void trustCheckPointDetector(String trustUID) {
        boolean isTrustLoggedIn = false;
        try {
            while (true) {
                String currentURL = driver.getDriver().getCurrentUrl();
                List<WebElement> fbNotificationsJewelList = driver.getDriver().findElements(By.id("fbNotificationsJewel"));
                if (currentURL.contains("checkpoint")) {
                    updateTrust(trustUID, "-");
                    break;
                } else if (!fbNotificationsJewelList.isEmpty()) {
                    updateTrust(trustUID, "=");
                    isTrustLoggedIn = true;
                    for(long i=0;i<10000000;i++){
                        try{
                            driver.getDriver().manage().timeouts().pageLoadTimeout(LOAD_TIME, TimeUnit.SECONDS);
                        }catch (Exception e){
                            break;
                        }
                        Thread.sleep(1000);
                    }
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {
        }finally {
            if(isTrustLoggedIn){
                updateTrust(trustUID, "+");
            }else {
                updateTrust(trustUID, "-");
            }
        }
    }

    private void updateTrust(String trustUID, String initSign) {
        if (baseAccountList.get(baseAccountListIndex).getTrust1() != null && baseAccountList.get(baseAccountListIndex).getTrust1().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust1(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust2() != null && baseAccountList.get(baseAccountListIndex).getTrust2().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust2(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust3() != null && baseAccountList.get(baseAccountListIndex).getTrust3().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust3(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust4() != null && baseAccountList.get(baseAccountListIndex).getTrust4().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust4(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust5() != null && baseAccountList.get(baseAccountListIndex).getTrust5().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust5(initSign + trustUID);
        }
        table.refresh();
    }
}
