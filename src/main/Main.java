package main;

import base.Driver;
import controllers.TableController;
import dto.LoginAccountBundleStcDTO;
import dto.WebLoginDTO;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import junit.framework.Assert;
import objects.Accounts;
import objects.CloneLoginImg;
import objects.userWebAccount;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import service.BaseService;
import util.common.DataUtil;
import dto.LoginAccountBundleStcDTO.TrustObject;

import java.util.List;

import static util.common.Constant.SERVER.SERVER_LOGIN_GET_INFO;
import static util.common.DataUtil.random;


public class Main extends Application {

    private Stage window;
    private TableView<Accounts> table;
    private TableView<CloneLoginImg> imgTable;
    private ObservableList<Accounts> cloneAccountList;
    private ObservableList<CloneLoginImg> cloneImgList;
    int cloneAccountListIndex, cloneImgListIndex;
    private TextField workingUIDInput, trustInput, employeeIdInput, employeePwInput, cloneInputField;
    private Button cloneLoginImgButton, skipButton, returnToHomeButton, cloneLoginAddNewButton, cloneLoginNextButton, employeeLoginButton, cloneLoginToolButton, unlockCheckPointButton, developingButton;
    private Button cloneLoginImgAddNewButton, cloneLoginImgNextButton, uploadAllPwAndEmail, imgReturnToHomeButton;
    private Driver driver;
    private int successTrust;
    private Scene authScene;
    private Scene homeScene;
    private Scene cloneLoginScene, cloneLoginImgScene;
    private Scene unlockCheckPointScene;
    private HBox authHBox, cloneLoginBottomHBox, cloneLoginTopHBox, homeHBox, unlockCheckPointHBox, cloneLoginImgBottomHBox, cloneLoginImgTopHBox;
    private VBox cloneLoginVBox, cloneLoginImgVBox;
    boolean whileCondition;
    public static String token;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));
        window = primaryStage;
        window.setTitle("THE RED DOG");
        Image image = new Image(getClass().getResourceAsStream("/main/resources/dogicon.png"));
        window.getIcons().add(image);
        driver = new Driver();


        // return to home Button

        returnToHomeButton = new Button("Home");
        returnToHomeButton.setMinHeight(30);
        returnToHomeButton.setMinWidth(100);
        returnToHomeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            returnToHomeButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        /**
         * authorization Scene
         */
        //employee username input
        employeeIdInput = new TextField();
        employeeIdInput.setPromptText("Username");
        employeeIdInput.setMinWidth(300);
        employeeIdInput.setMinHeight(40);

        //employee password input
        employeePwInput = new TextField();
        employeePwInput.setPromptText("Password");
        employeePwInput.setMinWidth(300);
        employeePwInput.setMinHeight(40);

        //employee Login Button
        employeeLoginButton = new Button("Đăng Nhập");
        employeeLoginButton.setMinHeight(40);
        employeeLoginButton.setMinWidth(100);
        employeeLoginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            employeeLoginButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //authorization HBox
        authHBox = new HBox();
        authHBox.setPadding(new Insets(20, 20, 20, 20));
        authHBox.setSpacing(50);
        authHBox.getChildren().addAll(employeeIdInput, employeePwInput, employeeLoginButton);

        //authorization scene
        authScene = new Scene(authHBox);

        //employeeLoginButton Action

        /**
         * home Scene
         */
        //clone login button
        cloneLoginToolButton = new Button("Clone Login By Trust");
        cloneLoginToolButton.setMinHeight(50);
        cloneLoginToolButton.setMinWidth(120);
        cloneLoginToolButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cloneLoginToolButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //clone login button
        cloneLoginImgButton = new Button("Clone Login By Image");
        cloneLoginImgButton.setMinHeight(50);
        cloneLoginImgButton.setMinWidth(120);
        cloneLoginImgButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cloneLoginImgButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //unlock Checkpoint button
        unlockCheckPointButton = new Button("Modify Clone");
        unlockCheckPointButton.setMinHeight(50);
        unlockCheckPointButton.setMinWidth(120);
        unlockCheckPointButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            unlockCheckPointButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //home HBox
        homeHBox = new HBox();
        homeHBox.setPadding(new Insets(30, 50, 30, 50));
        homeHBox.setSpacing(100);
        homeHBox.getChildren().addAll(cloneLoginToolButton, unlockCheckPointButton, cloneLoginImgButton);

        //home scene
        homeScene = new Scene(homeHBox);

        /**
         * clone login scene
         */
        TableController tc = new TableController();
        //workingUID collumn
        TableColumn<Accounts, String> workingUIDColumn = new TableColumn<>("Clone UID");
        tc.columnInitializer(workingUIDColumn, "workingUID", 220);

        //trust 1 collumn
        TableColumn<Accounts, String> trust1Column = new TableColumn<>("Trust 1 UID");
        tc.columnInitializer(trust1Column, "trust1", 170);

        //trust 2 collumn
        TableColumn<Accounts, String> trust2Column = new TableColumn<>("Trust 2 UID");
        tc.columnInitializer(trust2Column, "trust2", 170);

        //trust 3 collumn
        TableColumn<Accounts, String> trust3Column = new TableColumn<>("Trust 3 UID");
        tc.columnInitializer(trust3Column, "trust3", 170);

        //trust 4 collumn
        TableColumn<Accounts, String> trust4Column = new TableColumn<>("Trust 4 UID");
        tc.columnInitializer(trust4Column, "trust4", 170);

        //trust 5 collumn
        TableColumn<Accounts, String> trust5Column = new TableColumn<>("Trust 5 UID");
        tc.columnInitializer(trust5Column, "trust5", 170);

        //workingUID input
        workingUIDInput = new TextField();
        workingUIDInput.setPromptText("Clone UID");
        workingUIDInput.setMinWidth(150);
        workingUIDInput.setMinHeight(40);

        //trust input
        trustInput = new TextField();
        trustInput.setPromptText("Trust UID List");
        trustInput.setMinWidth(550);
        trustInput.setMinHeight(40);

        //add new button
        cloneLoginAddNewButton = new Button("Thêm Mới");
        cloneLoginAddNewButton.setMinHeight(40);
        cloneLoginAddNewButton.setMinWidth(100);
        cloneLoginAddNewButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cloneLoginAddNewButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //next button
        cloneLoginNextButton = new Button("Tiếp Theo");
        cloneLoginNextButton.disableProperty().setValue(true);
        cloneLoginNextButton.setMinHeight(40);
        cloneLoginNextButton.setMinWidth(100);
        cloneLoginNextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cloneLoginNextButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //skip button
        skipButton = new Button("Bỏ Qua");
        skipButton.disableProperty().setValue(true);
        skipButton.setMinHeight(40);
        skipButton.setMinWidth(100);
        skipButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            skipButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        //clone login top HBox
        cloneLoginTopHBox = new HBox();
        cloneLoginTopHBox.setPadding(new Insets(20, 20, 20, 20));
        cloneLoginTopHBox.setSpacing(50);
        cloneLoginTopHBox.getChildren().addAll(returnToHomeButton);

        //clone login bottom HBox
        cloneLoginBottomHBox = new HBox();
        cloneLoginBottomHBox.setPadding(new Insets(20, 40, 20, 40));
        cloneLoginBottomHBox.setSpacing(30);
        cloneLoginBottomHBox.getChildren().addAll(workingUIDInput, trustInput, cloneLoginAddNewButton, cloneLoginNextButton);

        //table
        cloneAccountListIndex = 0;
        cloneAccountList = FXCollections.observableArrayList();
        table = new TableView<>();
        table.setItems(cloneAccountList);
        table.getColumns().addAll(workingUIDColumn, trust1Column, trust2Column, trust3Column, trust4Column, trust5Column);

        //clone login VBox
        cloneLoginVBox = new VBox();
        cloneLoginVBox.getChildren().addAll(cloneLoginTopHBox, table, cloneLoginBottomHBox);

        //clone login Scene
        cloneLoginScene = new Scene(cloneLoginVBox);

        /**
         * clone login IMG scene
         */

        AddCloneNormalScene addCloneNormalScene = new AddCloneNormalScene(token);
        cloneLoginImgScene = addCloneNormalScene.display(window, homeScene);

        /**
         * unlock checkpoint scene
         */
        developingButton = new Button("Cái Này Chưa Làm Đâu");
        developingButton.setMinHeight(300);
        developingButton.setMinWidth(300);
        developingButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            developingButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        //unlock checkpoint scene
        ModifyCloneScene modifyCloneScene = new ModifyCloneScene(token);
        unlockCheckPointScene = modifyCloneScene.display(window, homeScene);

        window.setScene(authScene);
        window.show();
    }

    private void cloneLoginImgAddNewButtonClicked(){
        //TODO
        cloneImgList.forEach(x ->{
            System.out.println(x.getEmail());
        });
    }

    private void cloneLoginImgNextButtonClicked(){
        //TODO
    }

    private void uploadAllPwAndEmailClicked(){
        //TODO
    }

    public void skipButtonClicked() throws Exception {
        whileCondition = false;
    }

    public void returnToHomeButtonClicked() throws Exception {
        window.setAlwaysOnTop(false);
        window.setScene(homeScene);
    }

    public void developingButtonClicked() throws Exception {
        window.setAlwaysOnTop(false);
        window.setScene(homeScene);
    }

    public void cloneLoginToolButtonClicked() throws Exception {
        window.setScene(cloneLoginScene);
    }

    public void cloneLoginImgButtonClicked() throws Exception {
        window.setAlwaysOnTop(true);
//        window.setOpacity(0.9);
        window.setScene(cloneLoginImgScene);

    }

    public void unlockCheckPointButtonClicked() throws Exception {
        window.setAlwaysOnTop(true);
//        window.setOpacity(0.9);
        window.setScene(unlockCheckPointScene);
    }

    public void employeeLoginButtonClicked() throws Exception {
        BaseService baseService = new BaseService();

        userWebAccount loginWebAccount = new userWebAccount();
//        employeeIdInput.setText("defaultadmin");
//        employeePwInput.setText("1");
        loginWebAccount.setUsername(employeeIdInput.getText());
        loginWebAccount.setPassword(employeePwInput.getText());

        WebLoginDTO webLoginDTO = new WebLoginDTO();
        webLoginDTO = baseService.logInToWebServer(loginWebAccount);

        AlertBox alertBox = new AlertBox();
        alertBox.display(window,"Login Status", webLoginDTO.getMsg());

        if(webLoginDTO.isSuccess()){
            token = webLoginDTO.getToken();
            window.setScene(homeScene);
        }
//
    }

    private void cloneLoginAddNewButtonClicked() throws Exception {
        if (workingUIDInput.getText() != null && workingUIDInput.getText().length() > 5) {
            Accounts accounts = new Accounts();

            //working UID input
            accounts.setWorkingUID(workingUIDInput.getText());

            //trust UID input
            if (trustInput.getText() != null) {
                String trustUIDInput = trustInput.getText();
                String[] trustUIDList = trustUIDInput.split("[^0-9]");
                for (String trustUID : trustUIDList) {
                    setAccountsObject(trustUID, accounts);
                }
            }

            cloneAccountList.add(accounts);
            table.refresh();
            workingUIDInput.clear();
            trustInput.clear();
            cloneLoginNextButton.disableProperty().setValue(false);
        }
    }


    private void setAccountsObject(String trustUID, Accounts accounts) {
        if (trustUID.length() > 5) {
            if (accounts.getTrust1() == null) {
                accounts.setTrust1(trustUID);
            } else if (accounts.getTrust2() == null) {
                accounts.setTrust2(trustUID);
            } else if (accounts.getTrust3() == null) {
                accounts.setTrust3(trustUID);
            } else if (accounts.getTrust4() == null) {
                accounts.setTrust4(trustUID);
            } else if (accounts.getTrust5() == null) {
                accounts.setTrust5(trustUID);
            }
        }
    }

    private void cloneLoginNextButtonClicked() throws Exception {
//        cloneLoginNextButton.disableProperty().setValue(true);
//        cloneLoginDoneButton.disableProperty().setValue(false);
        BaseService baseService = new BaseService();
        successTrust = 0;

        if (cloneAccountListIndex == cloneAccountList.size()) {
            cloneLoginNextButton.disableProperty().setValue(true);
        } else {
            LoginAccountBundleStcDTO loginAccountBundleStcDTO = new LoginAccountBundleStcDTO();
            //send accountsObject to SV
            try {
                loginAccountBundleStcDTO = (LoginAccountBundleStcDTO) DataUtil.jsonStringToObject(
                        baseService.sendPostRequest(SERVER_LOGIN_GET_INFO, DataUtil.objectToJsonString(cloneAccountList.get(cloneAccountListIndex))).getMessage(),
                        LoginAccountBundleStcDTO.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (loginAccountBundleStcDTO.getWorkingUID() != null && loginAccountBundleStcDTO.getWorkingUID().equals("na")) {
                cloneAccountList.get(cloneAccountListIndex).setWorkingUID("Acc Đã Có Trên Hệ Thống");
            } else {
                if (loginAccountBundleStcDTO.getProxyDTO().getProxyId() == -1) {
                    cloneAccountList.get(cloneAccountListIndex).setWorkingUID("HẾT - PROXY");
                    cloneAccountList.get(cloneAccountListIndex).setTrust1("GỌI - 01644517304");
                    cloneAccountList.get(cloneAccountListIndex).setTrust2("ĐỂ THÊM - PROXY");
                    cloneAccountList.get(cloneAccountListIndex).setTrust3("");
                    cloneAccountList.get(cloneAccountListIndex).setTrust4("");
                    cloneAccountList.get(cloneAccountListIndex).setTrust5("");
                } else {
                    /**
                     * vòng login trust
                     */
                    if (!DataUtil.isNullOrEmpty(loginAccountBundleStcDTO.getTrustObjectList())) {
                        for (TrustObject trustObject : loginAccountBundleStcDTO.getTrustObjectList()) {
                            if (successTrust == 3) {
                                break;
                            }
                            try {
                                if (!isTrustOpenning(trustObject.getTrust())) {
                                    TrustOpener trustOpener = new TrustOpener(cloneAccountList, cloneAccountListIndex, trustObject, table);
                                    trustOpener.start();
                                }
//                        trustHandler(trustObject, cloneAccountList.get(cloneAccountListIndex));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    /**
                     * vòng login clone
                     */
                    try {
                        CloneOpener cloneOpener = new CloneOpener(cloneAccountList, cloneAccountListIndex, loginAccountBundleStcDTO.getProxyDTO(), loginAccountBundleStcDTO.getWorkingUID(), loginAccountBundleStcDTO.getProxyDTO().getProxyId(), table);
                        cloneOpener.start();
//                    driver.driverWithProxy(loginAccountBundleStcDTO.getProxyDTO());
//                    waitForPageLoaded();
//                    insertUID(loginAccountBundleStcDTO.getWorkingUID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        table.refresh();
        cloneAccountListIndex = cloneAccountListIndex + 1;
    }


    private void insertUID(String username) throws Exception {
        try {
            WebElement uidInputElements = driver.getDriver().findElement(By.xpath(".//input[@data-testid='royal_email']"));
            uidInputElements.sendKeys(username);
            Thread.sleep(random(20, 50));
        } catch (Exception ingnored) {
            System.out.println("insert Failed");
        }
    }

    private boolean waitForPageLoaded() {
        WebDriverWait wait = new WebDriverWait(driver.getDriver(), 60);
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return ((JavascriptExecutor) webDriver).executeScript("return document.readyState").toString().equals("complete");
            }
        };
        try {
            Thread.sleep(1000);
            wait.until(expectation);
            Thread.sleep(1000);
            return true;
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
            return false;
        }
    }

    private void trustHandler(TrustObject trustObject, Accounts accounts) throws Exception {
        if (trustObject.getCookies().equals("na")) {
            driver.driverFor3G();
            insertUID(trustObject.getTrust());
        } else {
            driver.driverWithProxyAndCookie(trustObject.getProxyDTO(), trustObject.getCookies());
        }
    }

    private void trustCheckPointDetector(String trustUID, Accounts accounts, String trustLogType) {
        whileCondition = true;
        skipButton.disableProperty().setValue(false);
        try {
            while (whileCondition) {
                String currentURL = driver.getDriver().getCurrentUrl();
                List<WebElement> fbNotificationsJewelList = driver.getDriver().findElements(By.id("fbNotificationsJewel"));
                if (currentURL.contains("checkpoint")) {
                    String trustUIDHandled = trustUID + "_";
                    setAccountsObject(trustUIDHandled, accounts);
                    break;
                } else if (!fbNotificationsJewelList.isEmpty()) {
                    String trustUIDHandled = trustUID + "-";
                    if (trustLogType.equals("cookie")) {
                        trustUIDHandled = trustUID + "+";
                    }
                    successTrust = successTrust + 1;
                    setAccountsObject(trustUIDHandled, accounts);
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {

        }
    }

    private boolean isTrustOpenning(String trustUID) {
        for (Accounts accounts : cloneAccountList) {
            if (accounts.getWorkingUID() != null
                    && accounts.getWorkingUID().contains(trustUID)) {
                if (accounts.getWorkingUID().contains("=") || accounts.getWorkingUID().contains("-")) {
                    return true;
                }
            } else if (accounts.getTrust1() != null
                    && accounts.getTrust1().contains(trustUID)
                    ) {
                if (accounts.getTrust1().contains("=") || accounts.getTrust1().contains("-")) {
                    return true;
                }
            } else if (accounts.getTrust2() != null
                    && accounts.getTrust2().contains(trustUID)
                    ) {
                if (accounts.getTrust2().contains("=") || accounts.getTrust2().contains("-")) {
                    return true;
                }
            } else if (accounts.getTrust3() != null
                    && accounts.getTrust3().contains(trustUID)
                    ) {
                if (accounts.getTrust3().contains("=") || accounts.getTrust3().contains("-")) {
                    return true;
                }
            } else if (accounts.getTrust4() != null
                    && accounts.getTrust4().contains(trustUID)
                    ) {
                if (accounts.getTrust4().contains("=") || accounts.getTrust4().contains("-")) {
                    return true;
                }
            } else if (accounts.getTrust5() != null
                    && accounts.getTrust5().contains(trustUID)
                    ) {
                if (accounts.getTrust5().contains("=") || accounts.getTrust5().contains("-")) {
                    return true;
                }
            }
        }
        return false;
    }
}
