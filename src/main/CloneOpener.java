package main;

import base.Driver;
import dto.LoggedAccountDTO;
import dto.LoginAccountBundleStcDTO;
import dto.ProxyDTO;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import objects.Accounts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import service.BaseService;
import util.common.DataUtil;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static util.common.Constant.ACTION.LOAD_TIME;
import static util.common.Constant.SERVER.SERVER_ACCOUNT_REPORT_V2;

public class CloneOpener extends Thread implements Runnable {
    ObservableList<Accounts> baseAccountList;
    int baseAccountListIndex;
    ProxyDTO proxyDTO;
    String workingUID;
    TableView<Accounts> table;
    long workingProxyId;
    Driver driver;

    public CloneOpener(ObservableList<Accounts> baseAccountList, int baseAccountListIndex, ProxyDTO proxyDTO, String workingUID, long workingProxyId, TableView<Accounts> table) {
        this.baseAccountList = baseAccountList;
        this.baseAccountListIndex = baseAccountListIndex;
        this.table = table;
        this.proxyDTO = proxyDTO;
        this.workingUID = workingUID;
        this.workingProxyId = workingProxyId;
    }

    @Override
    public void run() {
        try {
            driver = new Driver();
            driver.driverWithProxy(proxyDTO);
            WebElement uidInputElements = driver.getDriver().findElement(By.xpath(".//input[@data-testid='royal_email']"));
            uidInputElements.sendKeys(workingUID);
            cloneCheckPointDetector(workingUID);
        } catch (Exception ignored) {
        }
    }

    private void cloneCheckPointDetector(String trustUID) {
        boolean isTrustLoggedIn = false;
        try {
            for (int i=0;i<1000000;i++) {
                String currentURL = driver.getDriver().getCurrentUrl();
                List<WebElement> fbNotificationsJewelList = driver.getDriver().findElements(By.id("fbNotificationsJewel"));
                if (currentURL.contains("checkpoint")) {
                    updateClone(trustUID, "-");
                } else if (!fbNotificationsJewelList.isEmpty()) {
                    updateClone(trustUID, "=");
                    BaseService baseService = new BaseService();
                    LoggedAccountDTO loggedAccountDTO = new LoggedAccountDTO();
                    loggedAccountDTO.setLoggedAccount(workingUID);
                    loggedAccountDTO.setProxyId(workingProxyId);
                    String cookie = driver.getCookies();
                    loggedAccountDTO.setCookies(cookie);
                    baseService.sendPostRequest(SERVER_ACCOUNT_REPORT_V2, DataUtil.objectToJsonString(loggedAccountDTO));
                    isTrustLoggedIn = true;
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {
        } finally {
            if (isTrustLoggedIn) {
                updateClone(trustUID, "+");
            } else {
                updateClone(trustUID, "-");
            }
        }
    }

    private void updateClone(String trustUID, String initSign) {
        if (baseAccountList.get(baseAccountListIndex).getWorkingUID() != null && baseAccountList.get(baseAccountListIndex).getWorkingUID().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setWorkingUID(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust1() != null && baseAccountList.get(baseAccountListIndex).getTrust1().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust1(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust2() != null && baseAccountList.get(baseAccountListIndex).getTrust2().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust2(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust3() != null && baseAccountList.get(baseAccountListIndex).getTrust3().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust3(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust4() != null && baseAccountList.get(baseAccountListIndex).getTrust4().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust4(initSign + trustUID);
        } else if (baseAccountList.get(baseAccountListIndex).getTrust5() != null && baseAccountList.get(baseAccountListIndex).getTrust5().contains(trustUID)) {
            baseAccountList.get(baseAccountListIndex).setTrust5(initSign + trustUID);
        }
        table.refresh();
    }
}
