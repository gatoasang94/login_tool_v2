package main;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class AlertBox {
    public void display(Stage mainWindow, String title, String message){
        Stage window = new Stage();
        boolean oldMainWindowStage = mainWindow.isAlwaysOnTop();
        mainWindow.setAlwaysOnTop(false);
        window.setAlwaysOnTop(true);

        Image image = new Image(getClass().getResourceAsStream("/main/resources/dogicon.png"));
        window.getIcons().add(image);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(300);
        window.setMinHeight(200);

        Label label = new Label();
        label.setText(message);

        Button closeAlertButton = new Button("OK");
        closeAlertButton.setMinHeight(30);
        closeAlertButton.setMinWidth(80);
        closeAlertButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            closeAlertButtonClicked(mainWindow, window, oldMainWindowStage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        VBox layout = new VBox(20);
        layout.getChildren().addAll(label, closeAlertButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
    private void closeAlertButtonClicked(Stage mainWindow, Stage window, boolean oldMainWindowStage){
        window.setAlwaysOnTop(false);
        mainWindow.setAlwaysOnTop(oldMainWindowStage);
        window.close();
    }
}
