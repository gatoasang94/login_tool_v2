
package main;

import base.Driver;
import dto.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import objects.CloneInformation;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import service.BaseService;
import util.common.Constant;

import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static main.AddCloneNormalScene.getUidFromCookie;


public class ModifyCloneScene extends Main {
    public ModifyCloneScene(String token) {
        super.token = token;
    }

    private volatile Driver driver;
    private Button getCloneInfoButton, updateButton, imgCheckPointSupportButton, cmtCheckPointSupportButton;
    private Button ibCheckPointSupportButton, returnHomeButton, cancelUpdateButton, changeProxyButton;
    private Button disableCloneButton, openBrowserButton;
    private TextField uidField, passwordField, emailField, backUpFileDir;
    private TextField realNameField, phoneField, birthdayField, addressField;
    private boolean isGoodCookie = false;
    private HBox uidHbox, pwEmailHbox, cmtIbCpHbox, changeHbox, imgHbox, returnHomeHbox, mainHbox;
    private HBox realNameHbox, birthdayHbox, phoneHbox, addressHbox;
    private VBox rightVbox, leftVbox;
    String workingCookie;
    Long workingProxyId;

    public Scene display(Stage window, Scene homeScene) {

        getCloneInfoButton = new Button("GET");
        getCloneInfoButton.setMinHeight(30);
        getCloneInfoButton.setMinWidth(100);
        getCloneInfoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getCloneInfoButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        updateButton = new Button("Cập Nhật");
        updateButton.setMinHeight(30);
        updateButton.setMinWidth(100);
        updateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            updateButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        imgCheckPointSupportButton = new Button("Mở CheckPoint Bằng Ảnh");
        imgCheckPointSupportButton.setMinHeight(30);
        imgCheckPointSupportButton.setMinWidth(100);
        imgCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            imgCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        cmtCheckPointSupportButton = new Button("Mở CheckPoint Bằng Bình Luận");
        cmtCheckPointSupportButton.setMinHeight(30);
        cmtCheckPointSupportButton.setMinWidth(100);
        cmtCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cmtCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        ibCheckPointSupportButton = new Button("Mở CheckPoint Bằng Tin Nhắn");
        ibCheckPointSupportButton.setMinHeight(30);
        ibCheckPointSupportButton.setMinWidth(100);
        ibCheckPointSupportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ibCheckPointSupportButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        returnHomeButton = new Button("Home");
        returnHomeButton.setMinHeight(30);
        returnHomeButton.setMinWidth(100);
        returnHomeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            returnHomeButtonClicked(window, homeScene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        cancelUpdateButton = new Button("Cancel");
        cancelUpdateButton.setMinHeight(30);
        cancelUpdateButton.setMinWidth(100);
        cancelUpdateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            cancelUpdateButtonClicked();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        changeProxyButton = new Button("Đổi Proxy");
        changeProxyButton.setMinHeight(30);
        changeProxyButton.setMinWidth(100);
        changeProxyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            changeProxyButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        disableCloneButton = new Button("DISABLE CLONE");
        disableCloneButton.setMinHeight(30);
        disableCloneButton.setMinWidth(100);
        disableCloneButton.setStyle("-fx-background-color: #ff090a");
        disableCloneButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            disableCloneButtonClicked(window, token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        openBrowserButton = new Button("Mở Browser");
        openBrowserButton.setMinHeight(30);
        openBrowserButton.setMinWidth(100);
        openBrowserButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            openBrowserButtonClicked(window);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        uidField = new TextField();
        uidField.setPromptText("Facebook UID");
        uidField.setMinWidth(200);
        uidField.setMinHeight(30);

        passwordField = new TextField();
        passwordField.setPromptText("Password");
        passwordField.setMinWidth(150);
        passwordField.setMinHeight(30);

        emailField = new TextField();
        emailField.setPromptText("Email");
        emailField.setMinWidth(150);
        emailField.setMinHeight(30);

        backUpFileDir = new TextField();
        backUpFileDir.setPromptText("Đường dẫn file backup *.html");
        backUpFileDir.setMinWidth(250);
        backUpFileDir.setMinHeight(30);

        phoneField = new TextField();
        phoneField.setPromptText("Số điện thoại");
        phoneField.setMinWidth(250);
        phoneField.setMinHeight(30);

        birthdayField = new TextField();
        birthdayField.setPromptText("Ngày Sinh");
        birthdayField.setMinWidth(250);
        birthdayField.setMinHeight(30);

        addressField = new TextField();
        addressField.setPromptText("Địa Chỉ");
        addressField.setMinWidth(250);
        addressField.setMinHeight(30);

        realNameField = new TextField();
        realNameField.setPromptText("Tên");
        realNameField.setMinWidth(250);
        realNameField.setMinHeight(30);

        birthdayField.setEditable(false);
        addressField.setEditable(false);
        realNameField.setEditable(false);
        phoneField.setEditable(false);
        passwordField.setEditable(false);
        emailField.setEditable(false);


        returnHomeHbox = new HBox();
        returnHomeHbox.setPadding(new Insets(10, 20, 10, 20));
        returnHomeHbox.setSpacing(20);
        returnHomeHbox.getChildren().addAll(returnHomeButton);

        uidHbox = new HBox();
        uidHbox.setPadding(new Insets(10, 20, 10, 20));
        uidHbox.setSpacing(20);
        uidHbox.getChildren().addAll(uidField, getCloneInfoButton, cancelUpdateButton);

        pwEmailHbox = new HBox();
        pwEmailHbox.setPadding(new Insets(10, 20, 10, 20));
        pwEmailHbox.setSpacing(20);
        pwEmailHbox.getChildren().addAll(passwordField, emailField, updateButton);

        changeHbox = new HBox();
        changeHbox.setPadding(new Insets(10, 20, 10, 20));
        changeHbox.setSpacing(50);
        changeHbox.getChildren().addAll(changeProxyButton, disableCloneButton);

        cmtIbCpHbox = new HBox();
        cmtIbCpHbox.setPadding(new Insets(10, 20, 10, 20));
        cmtIbCpHbox.setSpacing(20);
        cmtIbCpHbox.getChildren().addAll(cmtCheckPointSupportButton, ibCheckPointSupportButton);

        imgHbox = new HBox();
        imgHbox.setPadding(new Insets(10, 20, 10, 20));
        imgHbox.setSpacing(20);
        imgHbox.getChildren().addAll(backUpFileDir, imgCheckPointSupportButton);


        rightVbox = new VBox();
        rightVbox.getChildren().addAll(returnHomeHbox, uidHbox, pwEmailHbox, cmtIbCpHbox, imgHbox);

        realNameHbox = new HBox();
        realNameHbox.setPadding(new Insets(10, 20, 10, 20));
        realNameHbox.setSpacing(20);
        realNameHbox.getChildren().addAll(realNameField);

        birthdayHbox = new HBox();
        birthdayHbox.setPadding(new Insets(10, 20, 10, 20));
        birthdayHbox.setSpacing(20);
        birthdayHbox.getChildren().addAll(birthdayField);

        phoneHbox = new HBox();
        phoneHbox.setPadding(new Insets(10, 20, 10, 20));
        phoneHbox.setSpacing(20);
        phoneHbox.getChildren().addAll(phoneField);

        addressHbox = new HBox();
        addressHbox.setPadding(new Insets(10, 20, 10, 20));
        addressHbox.setSpacing(20);
        addressHbox.getChildren().addAll(addressField);

        leftVbox = new VBox();
        leftVbox.getChildren().addAll(realNameHbox, birthdayHbox, phoneHbox, addressHbox, changeHbox);

        mainHbox = new HBox();
        mainHbox.setPadding(new Insets(10, 20, 10, 20));
        mainHbox.setSpacing(20);
        mainHbox.getChildren().addAll(rightVbox, leftVbox);

        resetStage();
        Scene modifyCloneScene = new Scene(mainHbox);
        return modifyCloneScene;
    }

    private void getCloneInfoButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();

        CloneInformation cloneInformation = baseService.getCloneInformation(uidField.getText(), token);

        if (cloneInformation != null && cloneInformation.getCookies() != null) {
            workingStage();
            passwordField.setText(cloneInformation.getPassword());
            emailField.setText(cloneInformation.getEmail());
            phoneField.setText(cloneInformation.getPhone());
            birthdayField.setText(cloneInformation.getBirthday());
            addressField.setText(cloneInformation.getAddress());
            realNameField.setText(cloneInformation.getRealName());
            workingCookie = cloneInformation.getCookies();
            workingProxyId = cloneInformation.getProxyDTO().getProxyId();
            driver = new Driver();
            driver.driverWithProxyAndCookie(cloneInformation.getProxyDTO(), workingCookie);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    _updatePageTitle();
                }
            }).start();

        } else {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window, "Uid Problem", "Uid Không tồn tại");
        }
    }

    private String getPageSHA() throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(driver.getDriver().getPageSource().getBytes());
        return new String(messageDigest.digest());
    }

    private void updatePageTitle() {
        try {
            String oldSha = "";
            while (true) {
                String newSHA = getPageSHA();
                if (oldSha.equals(newSHA)) {
                    oldSha = newSHA;
                    String jsScriptCode = "document.title = '" + uidField.getText() + "'";
                    ((JavascriptExecutor) driver.getDriver()).executeScript(jsScriptCode);
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {

        }
    }

    private void _updatePageTitle() {
        try {
            String oldTitle = uidField.getText();
            while (true) {
                String newSHA = driver.getDriver().getTitle();
                if (!oldTitle.equals(newSHA)) {
                    String jsScriptCode = "document.title = '" + oldTitle + "'";
                    ((JavascriptExecutor) driver.getDriver()).executeScript(jsScriptCode);
                }
                Thread.sleep(1000);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private void updateButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();
        ModifiedCloneToSvDTO modifiedCloneToSvDTO = new ModifiedCloneToSvDTO();

        workingCookie = driver.getCookies();
        modifiedCloneToSvDTO.setUid(getUidFromCookie(workingCookie));
        modifiedCloneToSvDTO.setCookies(workingCookie);
        modifiedCloneToSvDTO.setPassword(passwordField.getText());
        modifiedCloneToSvDTO.setEmail(emailField.getText());
        modifiedCloneToSvDTO.setProxyId(workingProxyId);

        WebConnectionLog webConnectionLog = baseService.sendObjectToWebServer(modifiedCloneToSvDTO, Constant.WEB.WEB_ADDRESS + "/logintool/updateCloneInfo", token);

        if (webConnectionLog.isSuccess()) {
            resetStage();
            try {
                driver.getDriver().quit();
            } catch (Exception ignored) {
            }
        } else {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window, "Sending Problem", webConnectionLog.getMsg());
        }
    }

    private FriendImageCheckPointDTO getImageCheckPointObj(){
        FriendImageCheckPointDTO friendImageCheckPointDTO = new FriendImageCheckPointDTO();
        //get anh
        ImageCheckPointDtoToSv imageCheckPointDtoToSv = new ImageCheckPointDtoToSv();
        try{
            List<WebElement> imgSrcs = driver.getDriver().findElements(By.xpath(".//img[@class='img' && contains(@src, 'friend_name_image')]"));
            List<String> imgSrcText = new ArrayList<>();
            for(WebElement imgSrc: imgSrcs){
                imgSrcText.add(imgSrc.getAttribute("src"));
            }
            List<WebElement> friendNames = driver.getDriver().findElements(By.className("uiInputLabelLabel"));
            List<String> friendNameText = new ArrayList<>();
            for(WebElement friendName:friendNames){
                friendNameText.add(friendName.getText());
            }
            imageCheckPointDtoToSv.setFriendName(friendNameText);
            imageCheckPointDtoToSv.setImgSrc(imgSrcText);

            //todo gửi imageCheckPointDtoToSv lên sv, sv trả về friendImageCheckPointDTO

            return friendImageCheckPointDTO;
        } catch (Exception e){
            e.printStackTrace();
            friendImageCheckPointDTO.setSuggest("na");
            return friendImageCheckPointDTO;
        }
    }

    private String getHtmlSHA() {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(driver.getDriver().getPageSource().getBytes());
            return new String(messageDigest.digest());
        }catch (Exception ignored){
            return "na";
        }
    }

    private String waitUntilHtmlChange(String oldHtmlSha) throws InterruptedException {
        for(int i=0;i<20;i++){
            String newSha = getHtmlSHA();
            if(!oldHtmlSha.equals(newSha)){
                return newSha;
            }
            Thread.sleep(1000);
        }
        return oldHtmlSha;
    }
    private void checkPointHandleer() throws InterruptedException {
        String currentUrl = driver.getDriver().getCurrentUrl();
        //https://www.facebook.com/checkpoint/?next
        String currentSha = getHtmlSHA();
        if(currentUrl.endsWith("https://www.facebook.com/checkpoint/?next")){
            driver.getDriver().findElement(By.id("checkpointSubmitButton")).click();
            currentSha = waitUntilHtmlChange(currentSha);
            List<WebElement> verification_methods = driver.getDriver().findElements(By.xpath(".//input[@name='verification_method' && @value='3']"));
            //value=3 check point anh
            //value=4 checkpoint nhắn mã bảo mật về điện thoại
            if(!verification_methods.isEmpty()){
                verification_methods.get(0).click();//chọn mở cp bằng ảnh bb

                driver.getDriver().findElement(By.id("checkpointSubmitButton")).click(); //bấm tiếp tục
                currentSha = waitUntilHtmlChange(currentSha);
                driver.getDriver().findElement(By.id("checkpointSubmitButton")).click(); //bấm xác nhận tiếp tục
            }
        }
    }

    private void imgCheckPointSupportButtonClicked(Stage mainWindow) throws URISyntaxException {
        FriendImageCheckPointDTO friendImageCheckPointDTO = new FriendImageCheckPointDTO();
        friendImageCheckPointDTO = getImageCheckPointObj();
        if(!friendImageCheckPointDTO.getSuggest().equals("na")){
            AlertBox alertBox = new AlertBox();
            alertBox.display(mainWindow,"Tìm Thấy Bạn Bè", friendImageCheckPointDTO.getSuggest());
        } else {
            Stage imageWindow = new Stage();
            boolean oldMainWindowStage = mainWindow.isAlwaysOnTop();
            mainWindow.setAlwaysOnTop(false);
            imageWindow.setAlwaysOnTop(true);

            Image image = new Image(getClass().getResourceAsStream("/main/resources/dogicon.png"));
            imageWindow.getIcons().add(image);
            imageWindow.initModality(Modality.APPLICATION_MODAL);
            imageWindow.setTitle("Không Tìm Thấy Ảnh Vui Lòng Chọn Tay");
            imageWindow.setMinWidth(300);
            imageWindow.setMinHeight(200);

            VBox mainLayout = new VBox(20);

            for(FriendImageCheckPointDTO.FriendImageObject friendImageObject: friendImageCheckPointDTO.getFriendImageObjectList()){
                VBox subLayout = new VBox(20);
                subLayout.setPadding(new Insets(10, 20, 10, 20));
                HBox subHbox = new HBox();
                subHbox.setPadding(new Insets(10, 20, 10, 20));
                subHbox.setSpacing(20);

                HBox subButtonHbox = new HBox();
                subButtonHbox.setPadding(new Insets(10, 20, 10, 20));
                subButtonHbox.setSpacing(20);

                Button friendNameButton = new Button(friendImageObject.getFriendName());
                friendNameButton.setMinHeight(30);
                friendNameButton.setMinWidth(100);
                friendNameButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    friendNameButtonClicked(friendImageObject.getFriendProfileLink());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                subButtonHbox.getChildren().add(friendNameButton);

                for(String imageurl: friendImageObject.getImgUrl()){
                    Image imageObj = new Image(imageurl);
                    ImageView iv3 = new ImageView();
                    iv3.setImage(imageObj);
                    iv3.setFitWidth(100);
                    iv3.setPreserveRatio(true);
                    iv3.setSmooth(true);
                    iv3.setCache(true);

                    subHbox.getChildren().add(iv3);
                }


                subLayout.getChildren().addAll(friendNameButton, subHbox);
                mainLayout.getChildren().addAll(subLayout);
            }

            Scene scene = new Scene(mainLayout);
            imageWindow.setScene(scene);
            imageWindow.showAndWait();
        }
    }

    private void friendNameButtonClicked(String uri){
        getHostServices().showDocument(uri);
    }

    private void cmtCheckPointSupportButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window, "Thông Báo", "Chưa Làm Đâu :v");
    }

    private void ibCheckPointSupportButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window, "Thông Báo", "Chưa Làm Đâu :v");
    }

    private void returnHomeButtonClicked(Stage window, Scene homeScene) {
        window.setAlwaysOnTop(false);
        window.setScene(homeScene);
    }

    private void cancelUpdateButtonClicked() {
        resetStage();
    }

    private void changeProxyButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();

        ProxyDTO proxyDTO = baseService.changeCloneProxy(uidField.getText(), token);

        if (proxyDTO.getProxyId() != null) {
            workingStage();
            driver = new Driver();
            workingProxyId = proxyDTO.getProxyId();
            driver.driverWithProxyAndCookie(proxyDTO, workingCookie);
        } else {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window, "PROXY Problem", "HẾT PROXY - GỌI 01644517304");
        }
    }

    private void disableCloneButtonClicked(Stage window, String token) throws Exception {
        BaseService baseService = new BaseService();
        CloneUidToSvDTO cloneUidToSvDTO = new CloneUidToSvDTO();

        cloneUidToSvDTO.setUid(uidField.getText());

        WebConnectionLog webConnectionLog = baseService.sendObjectToWebServer(cloneUidToSvDTO, Constant.WEB.WEB_ADDRESS + "/logintool/disableclone", token);

        if (webConnectionLog.isSuccess()) {
            resetStage();
            try {
                driver.getDriver().quit();
            } catch (Exception ignored) {
            }
        } else {
            AlertBox alertBox = new AlertBox();
            alertBox.display(window, "Sending Problem", webConnectionLog.getMsg());
        }
    }

    private void openBrowserButtonClicked(Stage window) {
        AlertBox alertBox = new AlertBox();
        alertBox.display(window, "Thông Báo", "Chưa Làm Đâu :v");
    }

    private void workingStage() {
        uidField.setEditable(false);
        passwordField.setEditable(true);
        emailField.setEditable(true);

        getCloneInfoButton.disableProperty().setValue(true);
        updateButton.disableProperty().setValue(false);
        imgCheckPointSupportButton.disableProperty().setValue(false);
        cmtCheckPointSupportButton.disableProperty().setValue(false);
        ibCheckPointSupportButton.disableProperty().setValue(false);
        cancelUpdateButton.disableProperty().setValue(false);
        changeProxyButton.disableProperty().setValue(false);
        disableCloneButton.disableProperty().setValue(false);
        openBrowserButton.disableProperty().setValue(false);
    }

    private void resetStage() {
        uidField.setEditable(true);
        passwordField.setEditable(false);
        emailField.setEditable(false);
        workingCookie = null;

        passwordField.clear();
        emailField.clear();
        phoneField.clear();
        birthdayField.clear();
        addressField.clear();
        realNameField.clear();

        getCloneInfoButton.disableProperty().setValue(false);
        updateButton.disableProperty().setValue(true);
        imgCheckPointSupportButton.disableProperty().setValue(true);
        cmtCheckPointSupportButton.disableProperty().setValue(true);
        ibCheckPointSupportButton.disableProperty().setValue(true);
        cancelUpdateButton.disableProperty().setValue(true);
        changeProxyButton.disableProperty().setValue(true);
        disableCloneButton.disableProperty().setValue(true);
        openBrowserButton.disableProperty().setValue(true);
    }
}