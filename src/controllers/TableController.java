package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import objects.Accounts;

import java.net.URL;
import java.util.ResourceBundle;

public class TableController {

    /**
     * "=" cửa sổ đang mở
     * "+" log thành công và trong hệ thống có cookie
     * "-" checkpoint
     * @param column
     * @param cellKey
     * @param columnMinWidth
     */
    public void columnInitializer(TableColumn column, String cellKey, long columnMinWidth){
        column.setMinWidth(columnMinWidth);
        column.setCellValueFactory(new PropertyValueFactory<Accounts, String>(cellKey));
        column.setCellFactory(new Callback<TableColumn<Accounts, String>, TableCell<Accounts, String>>() {
            @Override
            public TableCell<Accounts, String> call(TableColumn<Accounts, String> param) {
                TableCell cell = new TableCell<Accounts, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
//                        System.out.println(getString());
                        setText(empty ? null : getString());
                        String cellText = getString();
                        if(cellText.contains("=")){
                            setStyle("-fx-background-color:#ffee00");
                        }else if(cellText.contains("+")){
                            setStyle("-fx-background-color:#14ff00");
                        }else if(cellText.contains("-")){
                            setStyle("-fx-background-color:#ff0000");
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem().toString();
                    }
                };
                return cell;
            }
        });
    }

}
