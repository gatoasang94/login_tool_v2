package base;

import dto.LoginAccountBundleStcDTO;
import dto.ProxyDTO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import util.common.DataUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static util.common.Constant.ACTION.LOAD_TIME;
import static util.common.Constant.DRIVER.*;

public class Driver {

    WebDriver driver;
    String extFolder;


    public void driverWithProxyAndCookie(ProxyDTO proxyDTO, String cookies) throws Exception {
        System.setProperty("webdriver.chrome.driver", CHROME_FOLDER);
        Proxy proxy = new Proxy();
        proxy.setHttpProxy(proxyDTO.getAddress());
        proxy.setSslProxy(proxyDTO.getAddress());
        proxy.setHttpProxy(proxyDTO.getAddress());

        String extFolder = initExtFolder();
        this.extFolder = extFolder;

        changeProxyAuth(extFolder, proxyDTO.getUsername(), proxyDTO.getPassword());

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("proxy", proxy);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");
        options.addArguments("load-extension=" + extFolder);

        //block notification
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        options.setExperimentalOption("prefs", prefs);

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().pageLoadTimeout(LOAD_TIME, TimeUnit.SECONDS);

        goToPage(FACEBOOK_HOME);

        try {
            loadCookies(cookies);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("loadCookies error");
            throw e;
        }

        goToPage(FACEBOOK_HOME);
    }

    public void driverWithProxy(ProxyDTO proxyDTO) throws Exception {
        System.setProperty("webdriver.chrome.driver", CHROME_FOLDER);
        Proxy proxy = new Proxy();
        proxy.setHttpProxy(proxyDTO.getAddress());
        proxy.setSslProxy(proxyDTO.getAddress());
        proxy.setHttpProxy(proxyDTO.getAddress());

        String extFolder = initExtFolder();
        this.extFolder = extFolder;

        changeProxyAuth(extFolder, proxyDTO.getUsername(), proxyDTO.getPassword());

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("proxy", proxy);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");
        options.addArguments("load-extension=" + extFolder);

        //block notification
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        options.setExperimentalOption("prefs", prefs);

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().pageLoadTimeout(LOAD_TIME, TimeUnit.SECONDS);

        goToPage(FACEBOOK_HOME);
    }

    public void driverFor3G() throws Exception {
        System.setProperty("webdriver.chrome.driver", CHROME_FOLDER);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");

        //block notification
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        options.setExperimentalOption("prefs", prefs);

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().pageLoadTimeout(LOAD_TIME, TimeUnit.SECONDS);

        goToPage(FACEBOOK_HOME);
    }

    public boolean goToPage(String url) {
        try {
            driver.get(url);
            return true;
        } catch (Exception e) {
            System.out.println("proxy Lagging, loading exceed 60s");
            return false;
        }
    }

    public String getCookies() {
        StringBuilder sbd = new StringBuilder();
        for (Cookie cookie : driver.manage().getCookies()) {
            sbd.append(cookie.getName() + ":");
            sbd.append(cookie.getValue());
            sbd.append(";");
        }
        return sbd.toString();
    }

    private void loadCookies(String cookiesStr) throws Exception {
        cookiesStr = cookiesStr.replaceAll("=", ":").replaceAll(";", "\n");

        String[] cookies = cookiesStr.split("\n");
        if(cookies.length <3){
            cookies = cookiesStr.split(";");
        }
        for (String cookie : cookies) {
            try {
                driver.manage().addCookie(new Cookie(cookie.split(":")[0], cookie.split(":")[1], ".facebook.com", "/", null));
            } catch (Exception e) {
                int a = 0;
                e.printStackTrace();
            }
        }
    }

    private void changeProxyAuth(String extFolder, String username, String password) throws IOException {
        List<String> newLines = new ArrayList<>();
        String jsFilePath = extFolder + "\\js\\extension.js";
        for (String line : Files.readAllLines(Paths.get(jsFilePath), StandardCharsets.UTF_8)) {
            if (line.contains("/*username*/")) {
                line = replaceString(line, username);
            }
            if (line.contains("/*password*/")) {
                line = replaceString(line, password);
            }
            newLines.add(line);
        }
        Files.write(Paths.get(jsFilePath), newLines, StandardCharsets.UTF_8);
    }

    private String initExtFolder() {
        try {
            String destFolder = MAIN_FOLDER + "\\" + EXTENSIONS_FOLDER + "\\" + DataUtil.generateUniqueRandomString();
            FileUtils.copyDirectory(new File(MAIN_FOLDER + "\\" + PROTOTYPE_EXT_FOLDER), new File(destFolder));

            return destFolder;
        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }
    }

    private String replaceString(String line, String input) {
        String s = line.substring(line.indexOf("'") + 1, line.indexOf("'", line.indexOf("'") + 1));
        return line.replaceAll(s, input);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getExtFolder() {
        return extFolder;
    }

    public void setExtFolder(String extFolder) {
        this.extFolder = extFolder;
    }
}
