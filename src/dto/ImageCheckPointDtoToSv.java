package dto;

import java.util.List;

public class ImageCheckPointDtoToSv {
    List<String> imgSrc; //ảnh check Point
    List<String> friendName; // tên bạn bè để lựa chọn

    public List<String> getFriendName() {
        return friendName;
    }

    public void setFriendName(List<String> friendName) {
        this.friendName = friendName;
    }

    public List<String> getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(List<String> imgSrc) {
        this.imgSrc = imgSrc;
    }
}
