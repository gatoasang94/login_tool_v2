package dto;

import java.util.List;

public class FriendImageCheckPointDTO {
    String suggest;
    //So Sánh chắc chắn thì trả về suggest = Nguyễn Văn A;
    //không giống thì trả về 'na' và friendImageObjectList;
    List<FriendImageObject> friendImageObjectList;

    public String getSuggest() {
        return suggest;
    }

    public void setSuggest(String suggest) {
        this.suggest = suggest;
    }

    public List<FriendImageObject> getFriendImageObjectList() {
        return friendImageObjectList;
    }

    public void setFriendImageObjectList(List<FriendImageObject> friendImageObjectList) {
        this.friendImageObjectList = friendImageObjectList;
    }

    public class FriendImageObject {
        String friendName; // tên
        String friendProfileLink; //địa chỉ trang cá nhân
        List<String> imgUrl; // url ảnh vd: "https://neilpatel-qvjnwj7eutn3.netdna-ssl.com/wp-content/uploads/2015/10/colors.jpg"

        public String getFriendProfileLink() {
            return friendProfileLink;
        }

        public void setFriendProfileLink(String friendProfileLink) {
            this.friendProfileLink = friendProfileLink;
        }

        public String getFriendName() {
            return friendName;
        }

        public void setFriendName(String friendName) {
            this.friendName = friendName;
        }

        public List<String> getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(List<String> imgUrl) {
            this.imgUrl = imgUrl;
        }
    }
}
