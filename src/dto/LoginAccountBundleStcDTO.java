package dto;

import java.util.List;

public class LoginAccountBundleStcDTO {

    String workingUID;
    ProxyDTO proxyDTO;
    List<TrustObject> TrustObjectList;

    public static class TrustObject {
        String trust;
        String cookies;
        ProxyDTO proxyDTO;

        public String getTrust() {
            return trust;
        }

        public void setTrust(String trust) {
            this.trust = trust;
        }

        public String getCookies() {
            return cookies;
        }

        public void setCookies(String cookies) {
            this.cookies = cookies;
        }

        public ProxyDTO getProxyDTO() {
            return proxyDTO;
        }

        public void setProxyDTO(ProxyDTO proxyDTO) {
            this.proxyDTO = proxyDTO;
        }
    }

    public String getWorkingUID() {
        return workingUID;
    }

    public void setWorkingUID(String workingUID) {
        this.workingUID = workingUID;
    }

    public ProxyDTO getProxyDTO() {
        return proxyDTO;
    }

    public void setProxyDTO(ProxyDTO proxyDTO) {
        this.proxyDTO = proxyDTO;
    }

    public List<TrustObject> getTrustObjectList() {
        return TrustObjectList;
    }

    public void setTrustObjectList(List<TrustObject> TrustObjectList) {
        this.TrustObjectList = TrustObjectList;
    }
}
